# vardot

Collection of my dotfiles, project models and other useful configuration files.
At least... useful for me.

gitconfig.local
: Setting for git.
: Copy in ~/.gitconfig.local.
: Add this to .gitconfig:
: ```
[include]
	path = ~/.gitconfig.local
```

On Windows, add `gitconfig.windows.local` too.

zshrc.local
: User version of zshrc. I always install [grml's zshrc](https://git.grml.org/?p=grml-etc-core.git;a=blob_plain;f=etc/zsh/zshrc;hb=HEAD).

vscode
: Settings for Visual Studio Code/Codium. At the moment I have only some snippets for C/C++.

makefile
: Collection of makefile for mcu projects.
