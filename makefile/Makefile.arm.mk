# Makefile ARM-specialized Rules for C/C++ software developement
# Requires Makefile.rules.mk v3
#
# Part of "one makefile to rule them all" by giuliof
# v3 rev0 - 29/09/2021

######################################################################
#                             SOURCES                                #
######################################################################

# Name of the project
PROJ_NAME    := main

TARGET_NAME  := $(PROJ_NAME).hex

## FILES ##
SRCS         := main.cpp

## Directories ##
## This is where the source files are located,
## which are not in the current directory
SRC_DIR      := .
INC_DIR      := $(SRC_DIR)

BUILD_DIR    := build
OUTPUT_DIR   := output

######################################################################
#                          ARCHITECTURE                              #
######################################################################

## GCC toolchain prefix
PREFIX := arm-none-eabi-

## Microcontroller -- ARM
MCU         := STM32F103xB
# MCU         := STM32L031xx
## cortex M0
ARCH_FLAGS := -mthumb -mcpu=cortex-m3 -march=armv7-m -D$(MCU)
# ARCH_FLAGS  := -mthumb -mcpu=cortex-m0plus -D$(MCU)
## Linker script
LDSCRIPT = core/linker/STM32F103C8Tx_FLASH.ld
# LDSCRIPT = core/linker/STM32F103C8Tx_SRAM.ld
# LDSCRIPT = core/linker/STM32L031F4Px_FLASH.ld

######################################################################
#                        TOOLCHAIN FLAGS                             #
######################################################################

## Compiler flags ##
## Debug informations
# COMMON_CFLAGS		= -g -gdwarf-2
## Optimisation
# COMMON_CFLAGS		+= -Og
COMMON_CFLAGS		+= -Os
## All warning messages
COMMON_CFLAGS		+= -Wall
COMMON_CFLAGS		+= -Wextra
COMMON_CFLAGS		+= -Wshadow
## Use smallest size for enums
# COMMON_CFLAGS		+= -fshort-enums

## gcc-specific flags ##
## Puts functions and data into its own section
CFLAGS			+= -ffunction-sections -fdata-sections

## g++-specific flags ##
CXXFLAGS		+= -std=c++11
# CXXFLAGS		+= -fno-exceptions
# CXXFLAGS		+= -fstack-usage
# CXXFLAGS		+= -fdump-tree-optimized
# CXXFLAGS		+= -ffunction-sections
# CXXFLAGS		+= -fdata-sections
# CXXFLAGS		+= -fno-threadsafe-statics

## Linker-specific flags ##
# LDFLAGS		+=

######################################################################
#                      PROGRAMMING TOOLS                             #
######################################################################

## With ST-Link
PRG      = st-flash
PRGFLAGS   = --format ihex write
## With stm32flash
# PRG        = stm32flash
# PRGPORT   ?= /dev/ttyUSB0
# PRGFLAGS   = $(PRGPORT)
# PRGFLAGS  += -w # mind the whitespace

######################################################################
#                             TARGETS                                #
######################################################################

include Makefile.rules.mk

## Put here other platform-specific rules

.PHONY: flash
flash: $(OUTPUT_DIR)/$(PROJ_NAME).hex
	@$(ECHO) -e "\033[1;36m[Flash      ]\033[0m $^"
	$(PRG) $(PRGFLAGS)$<
