# Makefile AVR-specialized Rules for C/C++ software developement
# Requires Makefile.rules.mk v3
#
# Part of "one makefile to rule them all" by giuliof
# v3 rev0 - 29/09/2021

######################################################################
#                             SOURCES                                #
######################################################################

# Name of the project
PROJ_NAME    := main

TARGET_NAME  := $(PROJ_NAME).hex

## FILES ##
SRCS         := main.cpp

## Directories ##
## This is where the source files are located,
## which are not in the current directory
SRC_DIR      := .
INC_DIR      := $(SRC_DIR)

BUILD_DIR    := build
OUTPUT_DIR   := output

######################################################################
#                          ARCHITECTURE                              #
######################################################################

## GCC toolchain prefix
PREFIX := avr-

## Microcontroller -- AVR
MCU        := atmega328p
F_CPU      := 16000000
ARCH_FLAGS := -mmcu=$(MCU) -DF_CPU=$(F_CPU)

######################################################################
#                        TOOLCHAIN FLAGS                             #
######################################################################

## Compiler flags ##
## Debug informations
# COMMON_CFLAGS		= -g -gdwarf-2
## Optimisation
# COMMON_CFLAGS		+= -Og
COMMON_CFLAGS		+= -Os
## All warning messages
COMMON_CFLAGS		+= -Wall
COMMON_CFLAGS		+= -Wextra
COMMON_CFLAGS		+= -Wshadow
## Use smallest size for enums
# COMMON_CFLAGS		+= -fshort-enums

## gcc-specific flags ##
## Puts functions and data into its own section
CFLAGS			+= -ffunction-sections -fdata-sections

## g++-specific flags ##
CXXFLAGS		+= -std=c++11
## Disable exceptions
CXXFLAGS		+= -fno-exceptions
CXXFLAGS		+= -fstack-usage
CXXFLAGS		+= -fdump-tree-optimized
CXXFLAGS		+= -ffunction-sections
CXXFLAGS		+= -fdata-sections
CXXFLAGS		+= -fno-threadsafe-statics

## Linker-specific flags ##
# LDFLAGS		+=

######################################################################
#                      PROGRAMMING TOOLS                             #
######################################################################

PRG        = avrdude
PRGPORT   ?= /dev/ttyUSB0
PRGFLAGS  += -c arduino -P $(PRGPORT) -p $(MCU) -U flash:w:

######################################################################
#                             TARGETS                                #
######################################################################

include Makefile.rules.mk

## Put here other platform-specific rules

.PHONY: flash
flash: $(OUTPUT_DIR)/$(PROJ_NAME).hex
	@$(ECHO) -e "\033[1;36m[Flash      ]\033[0m $^"
	@#$(PRG) $(PRGFLAGS) $<
	$(PRG) $(PRGFLAGS)$<
