# Makefile Basic Rules for C/C++ software developement
# Tunable for Embedded/Linux/BareMetalMCU/Libraries developement
#
# Part of "one makefile to rule them all" by giuliof
# v3 rev0 - 29/09/2021

######################################################################
#                        DEFAULT VALUES                              #
######################################################################

## Customize these variables including this makefile somewhere else

PROJ_NAME       ?= main
SRCS            ?= main.c

## Plain executable
TARGET_NAME	?= $(PROJ_NAME)
## Intel HEX
#TARGET_NAME	?= $(PROJ_NAME).hex
## Static library
#TARGET_NAME	?= lib$(PROJ_NAME).a
## Dynamic library
# TARGET_NAME	?= lib$(PROJ_NAME).so

SRC_DIR		?= .
INC_DIR		?= .
BUILD_DIR	?= build
OUTPUT_DIR	?= output

## GCC toolchain prefix
# PREFIX := avr-
# PREFIX := arm-none-eabi-

# ARCH_FLAGS	?= 

# LDSCRIPT	?=

######################################################################
#                        TOOLCHAIN FLAGS                             #
######################################################################

COMMON_CFLAGS   += $(ARCH_FLAGS)
COMMON_CFLAGS	+= $(addprefix -I,$(INC_DIR))
CFLAGS		+= $(COMMON_CFLAGS)
CXXFLAGS	+= $(COMMON_CFLAGS)
ifdef LDSCRIPT
LDFLAGS		+= -T$(LDSCRIPT)
endif

######################################################################
#                     PATHS AND PREREQUISITES                        #
######################################################################

## Object files
## Automatically declares object file names
OBJS         := $(patsubst %.c,   $(BUILD_DIR)/%.o,  $(filter %.c,$(SRCS)) )
OBJS         += $(patsubst %.cpp, $(BUILD_DIR)/%.o,  $(filter %.cpp,$(SRCS)) )
OBJS         += $(patsubst %.s,   $(BUILD_DIR)/%.o,  $(filter %.s,$(SRCS)) )
OBJS         += $(patsubst %.S,   $(BUILD_DIR)/%.o,  $(filter %.S,$(SRCS)) )

## Dependencies from .h/.hpp files
DEPS         := $(patsubst %.o, %.d, $(OBJS))

## Virtual Paths
## Tell make to look in that folder if it cannot find a source
## from the current directory
vpath %.c   $(SRC_DIR)
vpath %.cpp $(SRC_DIR)
vpath %.S   $(SRC_DIR)
vpath %.s   $(SRC_DIR)
vpath %.h   $(INC_DIR)

######################################################################
#                         SETUP TOOLS                                #
######################################################################
ECHO      := /bin/echo

## GCC/programming Tools
ifdef GCC_PATH
CC      = $(GCC_PATH)/$(PREFIX)gcc
CXX     = $(GCC_PATH)/$(PREFIX)g++
LD	= $(GCC_PATH)/$(PREFIX)ld
OBJCOPY = $(GCC_PATH)/$(PREFIX)objcopy
OBJDUMP = $(GCC_PATH)/$(PREFIX)objdump
GDB     = $(GCC_PATH)/$(PREFIX)gdb
AS      = $(GCC_PATH)/$(PREFIX)as
SIZE    = $(GCC_PATH)/$(PREFIX)size
else
CC      = $(PREFIX)gcc
CXX     = $(PREFIX)g++
LD      = $(PREFIX)ld
OBJCOPY = $(PREFIX)objcopy
OBJDUMP = $(PREFIX)objdump
GDB     = $(PREFIX)gdb
AS      = $(PREFIX)as
SIZE    = $(PREFIX)size
endif

######################################################################
#                             TARGETS                                #
######################################################################
all:     $(OUTPUT_DIR)/$(TARGET_NAME)

## Avoid generating deps if cleaning
## https://codereview.stackexchange.com/questions/2547/makefile-dependency-generation
ifneq ($(MAKECMDGOALS),clean)
-include $(DEPS)
endif

## asm ##
$(BUILD_DIR)/%.o : %.S
	@$(ECHO) -e "\033[1;33m[Assembling  ]\033[0m AS $<"
	@mkdir -p `dirname $@`
	$(CC) $(CFLAGS) -c $< -o $@

## asm ##
$(BUILD_DIR)/%.o : %.s
	@$(ECHO) -e "\033[1;33m[Assembling  ]\033[0m AS $<"
	@mkdir -p `dirname $@`
	$(CC) $(CFLAGS) -c $< -o $@

## .cxx files ##
$(BUILD_DIR)/%.o:  %.cpp
	@$(ECHO) -e "\033[1;33m[Compiling   ]\033[0m CX $<"
	@mkdir -p `dirname $@`
	$(CXX) $(CXXFLAGS) -c $< -o $@

## .c files ##
$(BUILD_DIR)/%.o:  %.c
	@$(ECHO) -e "\033[1;33m[Compiling   ]\033[0m CC $<"
	@mkdir -p `dirname $@`
	$(CC) $(CFLAGS) -c $< -o $@

.DELETE_ON_ERROR:
$(BUILD_DIR)/%.d: %.c
	@mkdir -p `dirname $@`
	@$(ECHO) -n "$@ " > $@
	@$(CC) -MM -MP -MT '$(@D)/$(basename $(<F)).o' $(CFLAGS) $< >> $@

.DELETE_ON_ERROR:
$(BUILD_DIR)/%.d: %.cpp
	@mkdir -p `dirname $@`
	@$(ECHO) -n "$@ " > $@
	@$(CC) -MM -MP -MT '$(@D)/$(basename $(<F)).o' $(CFLAGS) $< >> $@

## Elf or plain executable ##
$(OUTPUT_DIR)/$(PROJ_NAME).elf $(OUTPUT_DIR)/$(PROJ_NAME): $(OBJS)
	@$(ECHO) -e "\033[1;34m[Linking     ]\033[0m $@"
	@mkdir -p ${OUTPUT_DIR}
	$(LD) $(LDFLAGS) -o $@ $^ $(LDLIBS)
	@$(ECHO) -e "\033[1;35m[Disasm...   ]\033[0m $@"
	$(OBJDUMP) -S $@ > $(OUTPUT_DIR)/$(PROJ_NAME).lss

## Stripped binary ##
$(OUTPUT_DIR)/$(PROJ_NAME).bin: $(OUTPUT_DIR)/$(PROJ_NAME).elf
	@$(ECHO) -e "\033[1;36m[Binary      ]\033[0m $@"
	$(OBJCOPY) -O binary $< $@

## Intel HEX ##
$(OUTPUT_DIR)/$(PROJ_NAME).hex: $(OUTPUT_DIR)/$(PROJ_NAME).elf
	@$(ECHO) -e "\033[1;36m[Binary      ]\033[0m $@"
	$(OBJCOPY) -O ihex -R .eeprom $< $@

## Dynamic Library ##
$(OUTPUT_DIR)/lib$(PROJ_NAME).so: $(OBJS)
	@$(ECHO) -e "\033[1;36m[Dyn Library ]\033[0m $@"
	$(CC) $(LDFLAGS) -shared -o $@ $^ $(LDLIBS)

## Static Library ##
$(OUTPUT_DIR)/lib$(PROJ_NAME).a: $(OBJS)
	@$(ECHO) -e "\033[1;36m[Stat Library]\033[0m $@"
	$(AR) rcs $@ $^

.PHONY: size
size: $(OUTPUT_DIR)/$(PROJ_NAME).elf
	$(SIZE) $(OUTPUT_DIR)/$(PROJ_NAME).elf

.PHONY: clean
clean:
	@$(ECHO) -e "\033[1;33m[Cleaning   ]\033[0m"
	@rm -rf $(BUILD_DIR)/*
	@rm -rf $(OUTPUT_DIR)/*
